Basic data analysis for a class investigating whether mice prefer to interact with novel or familiar objects.
Includes t-tests, Anovas, boxplots, QQPlots, and summary statistics
